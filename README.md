# README #

This README would help you to clone and setup this web service on your local machine.

### What is this repository for? ###

* This SPA is implemented as part of an evaluation for Software developer role at Redspace. 
* This SPA is a single page application that allows the use to select -from a pre-populated list- a character from StarWarz universe and get detailed information about this character.
* It is depending on the starwarz-wiki service -single endpoint service- that returns back the detailed infor about the requested character. 
* Version 1.0

### How do I get set up? ###

* Make sure you have Node.js installed on your local machine
* Then clone this repo using this command on your command line git clone https://ahmed_waly5@bitbucket.org/ahmed_waly5/starwarz-viewer.git
* After it's done open the project folder using this command cd starwarz-viewer
* Then run this command to install all the dependencies on you machine npm install --save
* After everything is installed start the service using this command npm start
* Service would start autamtically on localhost:300
* Select from the drop down list your desired character and it will bring up the details for this charater.

### Developer note ###

* I was trying to add more styles to the app but i didn't like it at the end so i commented the initiation for that, if you would like to try it un-comment the line 59
    // setTimeout(this.nextBackground, 3000);


### Who do I talk to? ###

* Ahmed Waly
* ahmed.waly5@outlook.com